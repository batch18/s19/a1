function Pokemon(name, lvl, hp){
	this.name= name;
	this.level = lvl;
	this.health = hp;
	this.attack = lvl;
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name} for ${this.attack}`);
		console.log(`${target.name}'s hp is now reduced to ${target.health - this.attack}`);
		target.health = target.health - this.attack;
		if (target.health <= 10 ) {
			target.faint()
		}
		return target.health;
	}
	this.faint = function(){
		console.log(`${this.name} fainted`);
	}
	
}





let crobat = new Pokemon(`Crobat`, 20, 100);
let salazzle = new Pokemon(`Salazzle`, 10, 100);

crobat.tackle(salazzle)


